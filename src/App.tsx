import React, { Component } from 'react';
import { TableStore, Serializable } from 'zerodb-client';
import PropViewer from 'react-prop-viewer';

import logo from './logo.svg';
import './App.css';

export default class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      token: null,
      results: null,
      msg: "",
      serialized: null
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    this.setState({ msg: "Getting tokens..." });
    let val = await TableStore.getToken(
      "mytable",
      "https://keeptoken-cs.azurewebsites.net/api/GetSasToken",
    );
    let that = this;
    this.setState({ token: val, msg: "Token Retrieved!" });

    var data: Serializable = {
      partition: "p1",
      rowKey: "k1",
      string: "s",
      number: 123,
      mixedArray: [
        1,
        2,
        "string val",
        {
          partition: "arrayp3",
          rowKey: "arrayRowKey",
          subdata: "here"
        }
      ],
      obj: {
        partition: "subp1",
        rowKey: "subk1",
        some: "data",
        objLevel2: {
          partition: "lvl2",
          rowKey: "keyLvl2",
          some: "more data"
        }
      },
      objArray: [
        {
          partition: "arrayp3",
          rowKey: "arrayRowKey1",
          subdata: "here"
        },
        {
          partition: "arrayp3",
          rowKey: "arrayRowKey2",
          subdata: "there"
        },
        {
          partition: "arrayp3",
          rowKey: "arrayRowKey3",
          subdata: "everywhere"
        }
      ]
    };

    let store = new TableStore(this.state.token);
    await store.insertOrReplace(data);

    let result = await store.getByIdPair([
      {
        partition: "p1",
        rowKey: "k1"
      }
    ]);
    that.setState({ results: result });
  };

  render() {

    return [
      <PropViewer key="msg" message={this.state.msg} />,
      <PropViewer key="p1" token={this.state.token} />,
      <PropViewer key="p3" results={this.state.results} />
    ];
  }
}